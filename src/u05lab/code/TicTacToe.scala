package u05lab.code

import u05lab.code.TicTacToe.mergeBoards

object TicTacToe extends App {
  sealed trait Player{
    def other: Player = this match {case X => O; case _ => X}
    override def toString: String = this match {case X => "X"; case _ => "O"}
  }
  case object X extends Player
  case object O extends Player

  case class Mark(x: Double, y: Double, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Double, y: Double): Option[Player] = {
    var value:Option[Player] = Option.empty[Player]
    board.foreach(p => {
      if(p.x == x && p.y == y){
        value = Option(p.player)
      }
    })
    value
  }

  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    var toReturn:List[Board] = List()
    for( x<- 0 to 2;y<- 0 to 2){
      if(find(board,x,y).isEmpty){
        toReturn = toReturn.:+(Mark(x,y,player)::board)
      }
    }
    toReturn
  }

  def isOver(game: Game):Boolean={
    val rows:Boolean=(game.head.groupBy(mark=>(mark.x,mark.player)).filter(f=>f._2.size==3).nonEmpty)
    val columns:Boolean=(game.head.groupBy(mark=>(mark.y,mark.player)).filter(f=>f._2.size==3).nonEmpty)
    val d1:Boolean=(game.head.filter(m=> m.x==m.y).groupBy(mark=>mark.player).filter(f=>f._2.size==3).nonEmpty)
    val d2:Boolean=(game.head.filter(m=> m.x+m.y==2).groupBy(mark=>mark.player).filter(f=>f._2.size==3).nonEmpty)
    rows||columns||d1||d2
  }

  def mergeBoards(game:Game, boards:Seq[Board]):List[Game]= (boards,isOver(game)) match {
    case (_,true) => List(game)
    case (h::t,false) => (h::game)::mergeBoards(game,t)
    case _ => Nil
  }
  
  def computeAnyGame(player: Player, moves: Int): Stream[Game] = (player,moves) match {
    case (X,1)=> placeAnyMark(List(),X).map(x => List(x)).toStream
    case (O,1)=> placeAnyMark(List(),O).map(x => List(x)).toStream
    case (X,_)=>computeAnyGame(O,moves-1).flatMap(x=> mergeBoards(x,placeAnyMark(x.head,X)))
    case (O,_)=>computeAnyGame(X,moves-1).flatMap(x=> mergeBoards(x,placeAnyMark(x.head,O)))
  }


  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None

  printBoards(List(List(Mark(0,0,X)),List(Mark(1,1,O))))
  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O, 6) foreach {g => printBoards(g); println()}
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
}